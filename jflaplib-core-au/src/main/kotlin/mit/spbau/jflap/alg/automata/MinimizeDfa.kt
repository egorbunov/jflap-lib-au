package mit.spbau.jflap.alg.automata

import edu.duke.cs.jflap.automata.fsa.FiniteStateAutomaton
import edu.duke.cs.jflap.automata.fsa.Minimizer

/**
 * Created by Egor Gorbunov on 24.03.2016.
 * email: egor-mailbox@ya.ru
 */

fun getMinimizedDfa(dfa: FiniteStateAutomaton): FiniteStateAutomaton {
    val minimizer = Minimizer()
    minimizer.initializeMinimizer()
    var minimizableDfa = minimizer.getMinimizeableAutomaton(dfa) as FiniteStateAutomaton
    val tree = minimizer.getDistinguishableGroupsTree(dfa)
    return minimizer.getMinimumDfa(minimizableDfa, tree)
}
