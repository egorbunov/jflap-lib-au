jflap-lib-au
==============

That is [jflap-lib](https://github.com/citiususc/jflap-lib) with little extentions written in Kotlin. Just for lulz and homework at formal languages classes. 

See README for jflap-lib and all licencing stuff [here](https://github.com/citiususc/jflap-lib).