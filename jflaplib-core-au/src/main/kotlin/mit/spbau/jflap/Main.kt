package mit.spbau.jflap

import edu.duke.cs.jflap.file.XMLCodec
import mit.spbau.jflap.alg.automata.convertNfaToDfa
import mit.spbau.jflap.alg.automata.getMinimizedDfa
import mit.spbau.jflap.alg.regex.convertRegexToNDA
import mit.spbau.jflap.automata.layout
import mit.spbau.jflap.export.saveJFF
import java.io.File

/**
 * Created by Egor Gorbunov on 24.03.2016.
 * email: egor-mailbox@ya.ru
 */

fun main(args: Array<String>) {
    print("Hello, j-flap :)")
    val nda = convertRegexToNDA("a+b")
    nda.layout()
    val dfa = convertNfaToDfa(nda)
    dfa.layout()
    val minDfa = getMinimizedDfa(dfa)
    minDfa.layout()

    nda.saveJFF("nfa.jff")
    dfa.saveJFF("dfa.jff")
    minDfa.saveJFF("min_dfa.jff")
}