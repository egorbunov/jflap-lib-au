package mit.spbau.jflap.automata

import edu.duke.cs.jflap.automata.State
import edu.duke.cs.jflap.automata.fsa.FiniteStateAutomaton
import edu.duke.cs.jflap.automata.graph.AutomatonGraph
import edu.duke.cs.jflap.automata.graph.layout.GEMLayoutAlgorithm
import java.awt.Point

/**
 * Created by Egor Gorbunov on 23.03.16.
 * email: egor-mailbox@ya.ru
 */

fun FiniteStateAutomaton.createState(): State? {
    return createState(Point())
}

fun FiniteStateAutomaton.layout() {
    val g = AutomatonGraph(this)
    val alg = GEMLayoutAlgorithm()
    alg.layout(g, null)
    g.moveAutomatonStates()
}
