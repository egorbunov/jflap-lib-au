package mit.spbau.jflap.alg.regex

import edu.duke.cs.jflap.automata.fsa.FiniteStateAutomaton
import edu.duke.cs.jflap.regular.RegularExpression

/**
 * Created by Egor Gorbunov on 24.03.16.
 * email: egor-mailbox@ya.ru
 */

fun convertRegexToNDA(regexStr: String): FiniteStateAutomaton {
    val regex = RegularExpression(regexStr);
    val converter = RegexToFsaConverter(regex)
    return converter.convert()
}

