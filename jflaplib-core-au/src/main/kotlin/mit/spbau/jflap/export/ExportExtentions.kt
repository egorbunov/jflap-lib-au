package mit.spbau.jflap.export

import edu.duke.cs.jflap.automata.fsa.FiniteStateAutomaton
import edu.duke.cs.jflap.file.XMLCodec
import java.io.File

/**
 * Created by Egor Gorbunov on 24.03.2016.
 * email: egor-mailbox@ya.ru
 */

fun FiniteStateAutomaton.saveJFF(filename: String) {
    XMLCodec().encode(this, File(filename), null);
}