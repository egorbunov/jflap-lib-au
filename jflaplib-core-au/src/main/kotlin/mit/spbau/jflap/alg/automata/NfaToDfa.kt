package mit.spbau.jflap.alg.automata

import edu.duke.cs.jflap.automata.fsa.FiniteStateAutomaton
import edu.duke.cs.jflap.automata.fsa.NFAToDFA

/**
 * Created by Egor Gorbunov on 24.03.2016.
 * email: egor-mailbox@ya.ru
 */

fun convertNfaToDfa(nda: FiniteStateAutomaton): FiniteStateAutomaton {
    val converter = NFAToDFA()
    return converter.convertToDFA(nda)
}