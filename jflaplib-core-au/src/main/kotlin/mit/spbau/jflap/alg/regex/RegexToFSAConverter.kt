package mit.spbau.jflap.alg.regex

import edu.duke.cs.jflap.automata.State
import edu.duke.cs.jflap.automata.fsa.FSATransition
import edu.duke.cs.jflap.automata.fsa.FiniteStateAutomaton
import edu.duke.cs.jflap.regular.Discretizer
import edu.duke.cs.jflap.regular.RegularExpression
import mit.spbau.jflap.automata.*
import java.util.*

/**
 * Created by Egor Gorbunov on 24.03.16.
 * email: egor-mailbox@ya.ru
 */

class RegexToFsaConverter(regexpr: RegularExpression) {
    /** Regular expression **/
    private val regex = regexpr

    /** The FSA being built. */
    private var automaton = FiniteStateAutomaton()

    /** The set of transitions that still require expansion. */
    private var toDo = HashSet<FSATransition>()

    /** The set of lambda-transitions still unborn! */
    private var toDoTransitions = HashSet<FSATransition>()

    /** The current action, or 0 if no action. */
    private var action = 0

    /** The transition being replaced. */
    private var transition: FSATransition? = null

    /** The number of transitions needed for the current step. */
    private var transitionNeeded = 0

    /** The replacement transitions. */
    private var replacements: Array<FSATransition?>? = null

    /** For the concatenation. */
    private var catBeginMade = false
    private var catEndMade = false

    /**
     * The codes for actions on an expression, which are, in order, that
     * parentheses are to be taken out, that an expression is to be "destarred",
     * or that ors or concatenations are to be broken into their respective
     * parts.
     */
    private val DEPARENS = 1
    private val DESTAR = 1
    private val DEOR = 3
    private val DECAT = 4
    private val LAMBDA = "\u03BB" //empty string


    init {
        // initializing automation
        val initialState = automaton.createState()
        val finalState = automaton.createState()
        automaton.initialState = initialState
        automaton.addFinalState(finalState)
        val initialTransition = FSATransition(initialState,
                finalState, Discretizer.delambda(regex.asString().replace('!', LAMBDA[0])))
        automaton.addTransition(initialTransition)
    }

    fun convert(): FiniteStateAutomaton {
        var t = automaton.transitions[0] as FSATransition
        if (requiredAction(t.label) != 0)
            toDo.add(t)
        nextStep()
        completeAll()
        return automaton
    }

    /**
     * Does everything.
     */
    private fun completeAll() {
        while (action != 0 || toDo.size > 0)
            completeStep()
    }

    private fun completeStep() {
        if (action == 0) {
            val it = toDo.iterator()
            transitionCheck(it.next())
        }
        val from = transition!!.fromState
        val to = transition!!.toState
        when (action) {
            DEPARENS -> // Probably a deparenthesization, or whatever.
                return
            DEOR -> for (i in replacements!!.indices) {
                automaton.addTransition(lambda(from, replacements!![i]!!.fromState))
                automaton.addTransition(lambda(replacements!![i]!!.toState, to))
            }
            DECAT -> {
                automaton.addTransition(lambda(from, replacements!![0]!!.fromState))
                for (i in 0..replacements!!.size - 1 - 1)
                    automaton.addTransition(lambda(replacements!![i]!!.toState,
                            replacements!![i + 1]!!.fromState))
                automaton.addTransition(lambda(
                        replacements!![replacements!!.size - 1]!!.toState, to))
            }
            DESTAR -> {
                automaton.addTransition(lambda(from, replacements!![0]!!.fromState))
                automaton.addTransition(lambda(replacements!![0]!!.toState, to))
                automaton.addTransition(lambda(from, to))
                automaton.addTransition(lambda(to, from))
            }
        }
        transitionNeeded = 0
        nextStep()
    }

    /**
     * This will return the action that are necessary for a given subexpression.
     * If this method returns 0, that indicates that no action is required.
     *
     * @param expression
     *            the expression to check for actions that may be required
     */
    private fun requiredAction(expression: String): Int {
        if (expression.length <= 1)
            return 0
        if (Discretizer.or(expression).size > 1)
            return DEOR
        if (Discretizer.cat(expression).size > 1)
            return DECAT
        if (expression[expression.length - 1] == '*')
            return DESTAR
        if (expression[0] == '(' && expression[expression.length - 1] == ')')
            return DEPARENS
        throw IllegalArgumentException("Expression $expression not recognized!")
    }

    /**
     * Called when a transition is selected with the deexpressionifier tool.
     *
     * @param transition
     *            the transition that was clicked
     */
    private fun transitionCheck(transition: FSATransition) {
        if (action != 0) {
            return
        }
        action = requiredAction(transition.label)
        if (action == 0) {
            return
        }
        this.transition = transition
        toDo.remove(transition)
        val label = transition.label
        when (action) {
            DEPARENS -> {
                val s1 = transition.fromState
                val s2 = transition.toState
                val newLabel = Discretizer.delambda(label.substring(1, label.length - 1))
                automaton.removeTransition(transition)
                val t = FSATransition(s1, s2, newLabel)
                automaton.addTransition(t)
                if (requiredAction(newLabel) != 0)
                    toDo.add(t)
                action = 0 // That's all that need be done.
            }
            DESTAR -> {
                replacements = replaceTransition(transition,
                        arrayOf(Discretizer.delambda(label.substring(0,
                                label.length - 1))))
                transitionNeeded = 4
            }
            DEOR -> {
                replacements = replaceTransition(transition, Discretizer.or(label))
                transitionNeeded = 2 * replacements!!.size
            }
            DECAT -> {
                replacements = replaceTransition(transition, Discretizer.cat(label))
                transitionNeeded = replacements!!.size + 1
                catBeginMade = false
                catEndMade = false
            }
        }
        nextStep()
    }

    /**
     * Given a transition to replace, and a list of the strings the transition
     * is being broken into, modify the automaton so that that transition is
     * replaced with a sequence of transitions each corresponding to the array.

     * @param transition
     * *            the transition to replace
     * *
     * @param exps
     * *            the array of string expressions to replace the transition with
     * *
     * @return the array of transitions created
     */
    private fun replaceTransition(transition: FSATransition,
                                  exps: Array<String>): Array<FSATransition?>? {
        val t = arrayOfNulls<FSATransition>(exps.size)
        automaton.removeTransition(transition)
        for (i in exps.indices) {
            val s = automaton.createState()
            val e = automaton.createState()
            t[i] = FSATransition(s, e, exps[i])
            automaton.addTransition(t[i])
            if (requiredAction(t[i]!!.label!!) != 0)
                toDo.add(t[i]!!)
        }
        return t
    }

    /**
     * Creates a lambda-transition between two states.

     * @param from
     * *            the from state
     * *
     * @param to
     * *            the to state
     * *
     * @return a lambda-transition between those states
     */
    private fun lambda(from: State, to: State): FSATransition {
        return FSATransition(from, to, "")
    }

    private fun nextStep() {
        if (transitionNeeded == 0) {
            if (toDo.size > 0) {
                action = 0
                return
            }
            action = 0
            return
        }
    }
}
